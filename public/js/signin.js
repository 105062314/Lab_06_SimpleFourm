function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    // btnLogin.addEventListener('click', function () {
    //     /// TODO 2: Add email login button event
    //     ///         1. Get user input email and password to login
    //     ///         2. Back to index.html when login success
    //     ///         3. Show error message by "create_alert" and clean input field
    //     var email = txtEmail.value;
    //     var password = txtPassword.value;
    //     firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
    //         // Handle Errors here.
    //         var errorCode = error.code;
    //         var errorMessage = error.message;
    //     });
        
    //     var userLogin;
    //     firebase.auth().onAuthStateChanged(function(user) {
    //         if (user) {
    //             userLogin = user;
    //             console.log("User is logined", user)
    //             window.location.href = "index.html";
    //         } else {
    //             userLogin = null;
    //             console.log("User is not logined yet.");
    //         }
    //     });
    // });
    
    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(e)
        {
          create_alert("success",e.message);
          window.location.href = "index.html";
        }).catch(function(e)
        {
          create_alert("error",e.message);
          txtEmail.value = "";
          txtPassword.value = "";
        });
    });

    var provider = new firebase.auth.GoogleAuthProvider();

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href = "index.html";
        }).catch(function (error) {
            console.log('error: ' + error.message);
        });
    });

    btnSignUp.addEventListener('click', function () {        
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(result){
            txtEmail.value="";
            txtPassword.value="";
            create_alert("success",result);
        }).catch(function(error) {
            // Handle Errors here.
            txtEmail.value="";
            txtPassword.value="";
            create_alert("error",error)
            console.log(error);
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};